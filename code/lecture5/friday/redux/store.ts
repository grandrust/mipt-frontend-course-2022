import * as redux from "redux";

const initialState = {
    count: 0
};
const store = redux.createStore((state: any = initialState, action:redux.Action)=> {
    if (action.type === "increment") {
        return {
            ...state,
            count: state.count + 1
        }
    }

    if (action.type === "decrement") {
        return {
            ...state,
            count: state.count - 1
        }
    }
});

store.subscribe(()=> {
    const state = store.getState();

    console.log(`Current state;`);
    console.log(state);
});


store.dispatch({type: "increment"});
store.dispatch({type: "decrement"});