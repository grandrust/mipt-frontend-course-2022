import * as React from "react";
import { Table } from "./components/Table";

export function Hello() {
    const date: Date = new Date();
    return (
        <div>
            <h1>Hello World today is {date.toLocaleString()}</h1>
        </div>
    );
}

export default function App() {
    return (
        <Table />
    );
}
