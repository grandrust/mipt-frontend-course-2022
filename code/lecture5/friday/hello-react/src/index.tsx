import * as React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";

import App from "./App"; // const App = module.exports // module.exports = Hello
import store from "./store";

const root = createRoot(document.getElementById("root"));
root.render(
    <Provider store={store}>
        <App />
    </Provider>
);