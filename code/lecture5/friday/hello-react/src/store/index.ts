import { createSlice, configureStore, PayloadAction } from "@reduxjs/toolkit";

const initState: IProduct[] = [
  {
      id: 1,
      description: "desc1"
  },
  {
      id: 2,
      description: "desc2"
  },
  {
      id: 3,
      description: "desc3"
  }
];

const productSlice = createSlice({
  name: 'product',
  initialState: initState,
  reducers: {
    addProduct: (state: IProduct[], action: PayloadAction<IProduct> ) => {
      state.push(action.payload);
    }
  }
})

export const { addProduct } = productSlice.actions

// const store = configureStore({
//   reducer: counterSlice.reducer
// })


const store = configureStore(productSlice);

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export default store;