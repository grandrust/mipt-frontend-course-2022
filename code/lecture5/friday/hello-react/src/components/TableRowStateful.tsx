import * as React from "react";

interface ITableRowStatefulProps {
    count: number,
    description: string
}

interface ITableRowStatefulState {
    clickCount: number
}


export default class TableRowStateful extends React.Component<ITableRowStatefulProps, ITableRowStatefulState>  {
    constructor(props: ITableRowStatefulProps) {
        super(props);

        this.state = {
            clickCount: 0
            /// ..
        }

        // ....
    }

    componentDidMount(): void {
        console.log("componentDidMount");
        // fectchData()
    }

    componentWillUnmount(): void {
        console.log("componentWillUnmount");
    }

    onClickHandler(e) {
        this.setState(
            {
                ...this.state,
                clickCount: this.state.clickCount + 1,
            }
        );

        // currentState === newState
    }

    render() {
        return (
            <div className="table-row" onClick={(e) => this.onClickHandler(e)}>
            <div>{this.props.count}</div>
            <div>{this.props.description}</div>
            <div>Clicked {this.state.clickCount}</div>
        </div>
        );
    }
}