import * as React from "react";

interface ITableRowProps {
    count: number,
    description: string
}

export default function TableRow(props: ITableRowProps) {
    const [ clickCount, setClickCount ] = React.useState(0);
    const inputRef = React.useRef<HTMLInputElement>();

    // Side effect: React.useEffect
    // React.useEffect(async () => {}, )

    const onClickHandler = (e) => {
        setClickCount(clickCount + 1);

        console.log(inputRef.current.value);
    }
    return (
        <div className="table-row" onClick={onClickHandler}>
            <div>{props.count}</div>
            <div>{props.description}</div>
            <div>Clicked {clickCount}</div>
            <input type="text" ref={inputRef}></input>
        </div>);
}