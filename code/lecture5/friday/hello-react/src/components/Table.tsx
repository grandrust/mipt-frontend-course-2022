import * as React from "react";
import { useAppDispatch, useAppSelector } from "../hooks";

import TableRow from "./TableRow";
import TableRowStateful from "./TableRowStateful";
import { addProduct } from "../store";

import "./Table.css";

export function Table() {
    const dispatch = useAppDispatch();
    const products = useAppSelector(s => s);
    
    const onClickHandler = () => {
        const product: IProduct = {
            id: Math.random(),
            description: `New Product description ${new Date().getMilliseconds()}`
        };

        dispatch(addProduct(product));
    }

    return (
        <div className="table">
            <button onClick={onClickHandler}>Add New Product</button>
           {/* {data.map(item => <TableRow count={item.id} description={item.description} key={item.id} />)} */}
           {products.map(item => <TableRowStateful count={item.id} description={item.description} key={item.id} />)}
            {/* {data.map(item => <TableRow {...item} />)}  */}
        </div>
    );
}