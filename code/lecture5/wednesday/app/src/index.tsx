import * as React from "react";
import { createRoot, Root } from 'react-dom/client';
import { BrowserRouter } from "react-router-dom";

import "./styles.css";
import App from "./components/App";

const root: Root = createRoot(document.getElementById("root"));
root.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>
);