import * as React from "react";

interface IDataItemProps {
    description: string,
    count: number 
}

interface IDataItemState {
    count: number
}

export default class DataItem extends React.Component<IDataItemProps, IDataItemState> {
    constructor(props: IDataItemProps){
        super(props)

        this.state = {
            count: 0
        }
    }

    onClickHandler() {
        debugger;
        this.setState ({
            count: this.state.count + 1
        });
    }

    render() {
        const count = this.state.count;
        return (
        <div className="item" onClick={() => this.onClickHandler()}>
            <div className="item_index">{this.props.count}</div>
            <div className="item_desc">{this.props.description}</div>
            <div>{count}</div>
        </div>);
    }
}