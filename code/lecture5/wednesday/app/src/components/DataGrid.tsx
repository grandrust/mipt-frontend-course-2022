import * as React from "react";
import DataItem from "./DataItem";
import "./dataItem.css";

export default function DataGrid(props: React.PropsWithChildren) {
    const [elemts, setElemets] = React.useState([]);
    const [count, setCount] = React.useState(0);

    const createContext = () => {
        const data = [
            {
                description: "Some text",
                count: 1
            },
            {
                description: "Some text 13",
                count: 2
            },
            {
                description: "Some text 2",
                count: 3
            }
        ];

        let key = 0;
        return data.map(element => 
            <DataItem key={key++} {...element} />);
            // <DataItem description={element.description} count={element.count} />);
    }

    const addElement = () => {
        const element = <DataItem key={count} description="qwe" count={count} />;
        setCount(count+1);

        elemts.push(element);
        setElemets(elemts);
    }
    return (
        <div>
            {/* {createContext()} */}
            { ...elemts }
            {props.children}
            <button onClick={() => addElement()}>
                Add Item
            </button>
        </div>
    )
}