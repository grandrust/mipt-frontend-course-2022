import * as React from "react";
import DataRow from "./DataRow";


export default function DataTable() {
    const rows = [
        <DataRow key={1} description="Product 1" value={23}/>,
        <DataRow key={2} description="Product 2" value={100}/>
    ];

    const data = {
        products: [
            {
                name: "Product 1",
                cost: 23
            },
            {
                name: "Product 2",
                cost: 100
            },
            {
                name: "Product 3",
                cost: 1000
            },
            {
                name: "Product 4",
                cost: 300
            }
        ]
    }

    const data1 = {
        products: [
            {
                description: "Product 5",
                value: 230
            },
            {
                description: "Product 6",
                value: 10
            }
        ]
    }

    const renderRows = () => {
        return data.products.map((product, key) => 
            <DataRow key={key} description={product.name} value={product.cost} />
        );
    }

    const renderRows1 = () => {
        return data1.products.map((product, key) => 
            <DataRow key={key} {...product} />
        );
    }

    return (
        <div className="table">
            {/* {rows} */}
            {renderRows()}
            {renderRows1()}
        </div>
    );
}