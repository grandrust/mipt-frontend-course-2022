import * as React from "react";

// Doesn't work
// export default function Footer () {
//     let count = 0;
//     const changeCount = () => { debugger; count++; }
//     return (
//         <footer onClick={()=> changeCount()}>
//             Copyright ....
//             {(count > 5) ? "Hehehe" : "" }
//         </footer>
//     );
// }

// Use react hooks
export default function Footer () {
    const [count, setCount] = React.useState(0);
    const changeCount = () =>  { debugger; setCount(count+1); }

    return (
        <footer onClick={()=> changeCount()}>
            Copyright ....
            {(count > 5) ? "Hehehe" : "" }
        </footer>
    );
}

