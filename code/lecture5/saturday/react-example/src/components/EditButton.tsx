import * as React from "react";

interface IEditButtonProps {
    checked: boolean;
}

interface IEditButtonState {
    checked: boolean;
    changed_time: number;
}

export default 
class EditButton extends React.Component<IEditButtonProps, IEditButtonState> {
    constructor(props: IEditButtonProps) {
        super(props);

        this.state = {
            checked: props.checked,
            changed_time: 0
        }
    }

    changeState() {
        this.setState(
            {
                checked: !this.state.checked,
                changed_time: this.state.changed_time + 1
            }
        )
    }

    render() {
        const count = (this.state.changed_time > 0) ? this.state.changed_time : "";
        return (
            <>
                <input type="checkbox" checked={this.state.checked} 
                    onChange={() => this.changeState()} 
                />
                <span>{count}</span>
            </>
        );
    }
}