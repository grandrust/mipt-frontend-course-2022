import * as React from "react";
import EditButton from "./EditButton";

interface IDataRowProps {
    description: string;
    value: number;
}

export default function DataRow(props: IDataRowProps) {
    return(
        <div className="table-row">
            <div>{props.description}</div>
            <div>$ {props.value}</div>
            <div>
                <EditButton checked={false} />
            </div>
        </div>
        );
}