import * as React from "react";
import { Routes, Route, Link } from "react-router-dom";

import DataTable from "./DataTable";
import Footer from "./Footer";

function Home() {
    return (
            <main>
                <nav>
                    <Link to="/products">Products</Link>
                </nav>
                <h2>Welcome to the homepage!</h2>
            </main>
    );
}

function Products() {
    return (
            <main>
                <nav>
                    <Link to="/">Home</Link>
                </nav>
                <div>
                    <h1>Products</h1>
                    <DataTable />
                </div>
            </main>
    );
}


export default function App(props: React.PropsWithChildren) {
    return (
        <div>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/products" element={<Products />} />
            </Routes>
            {props.children}
            <Footer />
        </div>
    );
}