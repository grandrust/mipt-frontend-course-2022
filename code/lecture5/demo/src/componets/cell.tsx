import * as React from "react";

interface ICellProps {
    count: number
}

interface ICellState {
    clickCount: number
}

export class Cell extends React.Component<ICellProps, ICellState> {
    constructor(props) {
        super(props);

        this.state = {
            clickCount: 0
        };
      }

      onClickHandler() {
        this.setState(
            {
                clickCount: this.state.clickCount + 1
            }
        );

        console.log(`Clicks: ${this.state.clickCount}`);
      }
    
      render() {
        const clicks = this.state.clickCount;
        const context = (clicks >= this.props.count) ? ":((" : ":)";

        return (
          <button className="cell" onClick={(e) => this.onClickHandler()}>
              {context}
          </button>
        );
      }
}