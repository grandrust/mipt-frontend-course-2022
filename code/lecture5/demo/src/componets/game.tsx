import * as React from "react";
import { Cell } from "./cell";

export class Game extends React.Component {
    constructor(props) {
        super(props);
      }

      generateCells(size: number) {
          return [
            <Cell count={3} />,
            <br></br>,
            <Cell count={3} />,
            <br></br>,
            <Cell count={3} />,
          ];
      }
    
      render() {
        return (
          <div className="square">
           {this.generateCells(5)}
          </div>
        );
      }
}