import * as React from "react";
import * as ReactDOM from "react-dom";
import { Game } from "./componets/game";

ReactDOM.render(
    <Game />,
    document.getElementById('game')
);
