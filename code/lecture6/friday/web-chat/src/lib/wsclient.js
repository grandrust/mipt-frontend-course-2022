export default class WSClient
{
    constructor() {
        this.socket = new WebSocket("ws://localhost:3000");

        this.onMessage = (data) => console.log(data);

        this.socket.onopen = (e) => {
          console.log('Connection to server opened');
        }

        this.socket.onmessage = (ev) => {
          this.onMessage(ev.data);
          
          console.log(`Get from sw: ${ev.data}`);
        }
    }

    send(message) {
        this.socket.send(message);
    }
}