const http = require('http');
const Koa = require("Koa");
const serve = require('koa-static')
const path = require('path')
const app = new Koa();
const WebSocketServer = require('ws').Server;

// logger
app.use(async (ctx, next) => {
    await next();
    const rt = ctx.response.get('X-Response-Time');
    console.log(`${ctx.method} ${ctx.url} - ${rt}`);
});

// x-response-time
app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.set('X-Response-Time', `${ms}ms`);
});

// response
app.use(serve(path.join(__dirname, '/public')));

const server = http.createServer(app.callback());
const wss = new WebSocketServer({ server });


const clients = [];

wss.on('connection', (ws, request, client) => {
    clients.push(ws);

    ws.on('message', data => {
      console.log('received: %s', data);

      for (let client of clients)
      {
        client.send('' + data);
      } 
    });
  
    ws.send('Connected');
  });


server.listen(3000);