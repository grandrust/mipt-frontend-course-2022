import * as React from "react";
import * as ReactDOM from "react-dom";
import { Chat, IChatProps } from "./componets/chat";


const props: IChatProps = {
    messages: [ "Hello", "Welcome"]
}

ReactDOM.render(
    <Chat {...props} />,
    document.getElementById('root')
);
