import * as React from "react";
import { Message } from "./message";

export interface IChatProps {
  messages: string[]
}

interface IChatState {
  messages: string[]
}

class Controller
{
    private socket: WebSocket;
    public onMessage: (e: string) => void;

    constructor() {
        this.socket = new WebSocket("ws://localhost:3000");
        this.onMessage = (e) => console.log("The method isn't changed");

        this.socket.onopen = (e) => {
          console.log('Connection to server opened');
        }

        this.socket.onmessage = (ev: MessageEvent<string>) => {
          this.onMessage(ev.data);
          
          console.log(`Get from sw: ${ev.data}`);
        }
    }

    public send = (message:string) => 
    {
        this.socket.send(message);
    }
}


export class Chat extends React.Component<IChatProps, IChatState> {
    controller: Controller;

    constructor(props) {
        super(props);

        this.controller = new Controller();

        this.state = {
          messages: this.props.messages
        };
      }

      componentDidMount() {
        this.controller.onMessage = (e) => {
          const { messages } = this.state;

          messages.push(e);
          this.setState({ messages });
        }
      }

      generateContext() {
          return this.state.messages.map(
            (m: string, indx: number) => <Message text={m} key={indx} />
          );
      }

      getMessage(): string {
        return "Hello";
      }
    
      render() {
        const handler = this.controller.send;

        return (
          <div className="chat-container">
           { this.generateContext() }
           <button onClick={() => handler(this.getMessage())}>Send Message</button>
          </div>
        );
      }
}