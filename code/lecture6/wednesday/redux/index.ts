import * as redux from "redux";

const initialState = {
    count: 0
}

const action_dictionary = {
    action: () =>{}
}

function reducer1(s, a) => {
    action_dictionary[a.type](a.payload);
}

const reducer = (state: any = initialState, action: redux.AnyAction) => {
    if (action.type === "increment")
    {
        return {
            ...state,
            count: state.count + 1
        }
    }

    if (action.type === "decrement")
    {
        return {
            ...state,
            count: state.count - 1
        }
    }
}

const store = redux.createStore(reducer);

store.subscribe(() => {
    const state = store.getState();
    console.dir(state);
});

//store.count += 2;

store.dispatch({type: "decrement"});
store.dispatch({type: "decrement"});
store.dispatch({type: "increment", payload: 2});