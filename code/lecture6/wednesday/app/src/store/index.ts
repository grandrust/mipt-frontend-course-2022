import * as redux from "@reduxjs/toolkit";
import { productReducer } from "./products";

export const store = redux.configureStore(productReducer);