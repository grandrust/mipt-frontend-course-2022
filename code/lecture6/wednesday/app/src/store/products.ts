import * as redux from "@reduxjs/toolkit";

// {
//     "model1": {  // => reducer1 

//     },
//     "model2": { // reducer1

//     }
// }

export interface Product {
    id: string,
    name: string,
    cost: number
}

export interface ProductState {
    products: Product[]
}

export const productReducer = redux.createSlice({
    name: "products",
    initialState: {
        products: []
    },
    reducers: {
        load: (state, action: redux.PayloadAction<Product[]>) => {
            state.products = action.payload;
        },
        addProduct: (state, action: redux.PayloadAction<Product>) => {
            state.products.push(action.payload);
        }
    }
});

export const productsAction = productReducer.actions;