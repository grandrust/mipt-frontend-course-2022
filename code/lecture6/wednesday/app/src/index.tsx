import * as React from "react";
import { createRoot, Root } from 'react-dom/client';
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";

import "./styles.css";
import App from "./components/App";
import { store } from "./store";

const root: Root = createRoot(document.getElementById("root"));
root.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);