import * as React from "react";
import { connect } from "react-redux";

interface IDataItemProps {
    description: string,
    count: number 
}

interface IDataItemState {
    count: number
}

export default class DataItem extends React.Component<IDataItemProps, IDataItemState> {
    constructor(props: IDataItemProps){
        super(props)

        this.state = {
            count: 0
        }
    }

    onClickHandler() {
        this.setState ({
            count: this.state.count + 1
        });
    }

    render() {
        const count = this.state.count;
        return (
        <div className="item" onClick={() => this.onClickHandler()}>
            <div className="item_index">{this.props.count}</div>
            <div className="item_desc">{this.props.description}</div>
            <div>{count}</div>
        </div>);
    }
}

// const mapStateToProps = (state, ownProps) => ({
//     active: ownProps.filter === state.visibilityFilter
//   })
  
//   const mapDispatchToProps = (dispatch, ownProps) => ({
//     onClick: () => dispatch(setVisibilityFilter(ownProps.filter))
//   })
  
//   export default connect(
//     mapStateToProps,
//     mapDispatchToProps
//   )(Link)