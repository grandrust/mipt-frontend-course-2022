import * as React from "react";
import {useSelector, useDispatch} from "react-redux";

import { Product, productsAction, ProductState } from "../store/products";

import DataItem from "./DataItem";
import "./dataItem.css";

export default function DataGrid(props: React.PropsWithChildren) {
    const [elemts, setElemets] = React.useState([]);
    const [count, setCount] = React.useState(0);
    const products = useSelector<ProductState, Product[]>(state => state.products);
    const dispatch = useDispatch();


    let isInit = true;
// Load async data
//     React.useEffect(function () {
//         const loadData = async () => {
//             fetch(...);
//             return data;
//         };

//         const dispatch = await loadData();
//         dispatch(productsAction.load(dispatch));

// }, [products, dispatch]);

    const createContext = () => {
        return products.map(product => 
            <DataItem key={product.id} description={product.name} count={product.cost} />);
    }

    const addElement = () => {
        dispatch(productsAction.addProduct({
            id: Math.random().toString(),
            name: "New Product",
            cost: 123
        }))
    }
    return (
        <div>
            { createContext() }
            { ...elemts }
            {props.children}
            <button onClick={() => addElement()}>
                Add Item
            </button>
        </div>
    )
}