import * as React from "react";
import { createRoot } from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from "react-redux";

import App from "./components/App";
import { store } from "./store";

import "./styles.css";

const root = createRoot(document.getElementById("root"));
root.render(
    <Provider store={store}>
        <BrowserRouter>
            <App >
                <p>Hello</p>
            </App>
        </BrowserRouter>
    </Provider>
);
