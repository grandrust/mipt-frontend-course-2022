import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState: IProductsState = {
    products: [ ]
}

export const productsSlice = createSlice(
    {
        name: "products",
        initialState,
        reducers: {
            addProduct: (state: IProductsState, action: PayloadAction<IProduct>) => {
                // reduxjs/toolkit creates copy of the state
                state.products.push(action.payload);
            },
            uploadProducts: (state: IProductsState, action: PayloadAction<IProduct[]>) => {
                // reduxjs/toolkit creates copy of the state
                state.products = action.payload;
            }
        }
    }
);

export const productsActions = productsSlice.actions;