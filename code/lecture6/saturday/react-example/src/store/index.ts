import { configureStore } from "@reduxjs/toolkit";
import { productsSlice } from "./products";


export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export const store = configureStore(productsSlice);