import { AppDispatch } from "../store";
import { productsActions } from "../store/products";

export const loadProducts = () => 
    async (dispatch: AppDispatch) =>  
    {
        const response =  await fetch("http://localhost:9000/api/products");
        const products = await response.json();
        dispatch(productsActions.uploadProducts(products));
    }