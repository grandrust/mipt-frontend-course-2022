import * as React from "react";
import { Routes, Route, Link } from "react-router-dom";

import DataTable from "./DataTable";
import DataTableV2 from "./DataTableV2";
import Footer from "./Footer";

function Home() {
    return (
            <main>
                <nav>
                    <Link to="/products">Products</Link>
                    <Link to="/table">Table</Link>
                </nav>
                <h2>Welcome to the homepage!</h2>
            </main>
    );
}

function Products() {
    return (
            <main>
                <nav>
                    <Link to="/">Home</Link>
                </nav>
                <div>
                    <h1>Products</h1>
                    <DataTable />
                </div>
            </main>
    );
}

function Table() {
    return (
            <main>
                <nav>
                    <Link to="/">Home</Link>
                </nav>
                <div>
                    <h1>Products</h1>
                    <DataTableV2 />
                </div>
            </main>
    );
}


export default function App(props: React.PropsWithChildren) {
    return (
        <div>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/products" element={<Products />} />
                <Route path="/table" element={<Table />} />
            </Routes>
            {props.children}
            <Footer />
        </div>
    );
}