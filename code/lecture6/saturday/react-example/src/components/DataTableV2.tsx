import * as React from "react";
import { connect } from "react-redux";

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Container } from "@mui/material";

import { AppDispatch } from "../store"
import { productsActions } from "../store/products";
import { loadProducts } from "../api/productsApi";

interface ITableProps {
    dispatch: AppDispatch,
    products: IProduct[]
}

interface ITableState {
    products: IProduct[]
}

class DataTableV2 extends React.Component<ITableProps, ITableState> {
    constructor(props: ITableProps) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(loadProducts());
    } 

    addProduct() {
        const product: IProduct = {
            name: "New Product",
            cost: Math.random()
        }

        this.props.dispatch(productsActions.addProduct(product)); // { type: "addProduct", payload: product}
    }

    render() {
        return (
            <Container>
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 100 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell align="right">Cost</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                this.props.products.map((row, index) => (
                                    <TableRow
                                        key={index}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            {row.name}
                                        </TableCell>
                                        <TableCell align="right">{row.cost}</TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <button onClick={() => this.addProduct()}>Click</button>
            </Container>
        )
    }
}

function mapStateToProps(state: IProductsState) {
    const { products } = state
    return { products };
}

export default connect(mapStateToProps)(DataTableV2)