import * as React from "react";
import { useAppSelector, useAppDispatch } from "../hooks";
import { productsActions } from "../store/products";

import DataRow from "./DataRow";


export default function DataTable() {
    const products = useAppSelector(state => state.products);
    const dispatch = useAppDispatch();

    const getDate = async () => {
        const response =  await fetch("http://localhost:9000/api/products");
        const products = await response.json();
        dispatch(productsActions.uploadProducts(products));
    }

    // let isInit = true;
    // if (isInit) {
    //     dispatch(getDate);
    //     isInit = false;
    // }
       
    React.useEffect(() => {
        // getDate();
    }, [dispatch]);

    const renderRows = () => {
        return products.map((product, key) => 
            <DataRow key={key} description={product.name} value={product.cost} />
        );
    }

    const addProduct = () => {
        const product: IProduct = {
            name: "New Product",
            cost: Math.random()
        }

        dispatch(productsActions.addProduct(product)); // { type: "addProduct", payload: product}
    }

    return (
        <div className="table">
            {renderRows()}
            <button onClick={addProduct}>Add New Product</button>
        </div>
    );
}