import * as redux from "redux";

const initialStore = {
    count: 0,
    products: {

    }
};
const reducer = (state: any = initialStore, action: redux.AnyAction) => {
    console.log("update store");

    // const actionDictionary = {
    //     "increment": (state) => {},
    //     "decrement": (state) => {}
    // }

    // actionDictionary[action.type];

    if (action.type === "increment")
    {
        // state.count++;
        // return state;

        return {
            ...state,
            count: state.count + 1
        }
    }
}
const store = redux.createStore(reducer);

store.subscribe(() => {
    const s = store.getState();
    // ...
    console.log("dispatch is called");
})

store.dispatch({type: "increment"})
store.dispatch({type: "decrement"})