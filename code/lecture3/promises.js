
const print = (message) => console.log(message);

async function todo() {
    var number = await Promise.resolve(123)
                                .then((value) => value+1)
                                .then((value) => value+1);


    // const http = {};

    // const ivan = await http.get("/student?query=name%like%Ivan");
    // const age = await http.get("age?name="+ivan);
    // number += age;

    print(number);
}

todo();
