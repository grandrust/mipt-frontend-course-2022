const { print } = require("./utils");

print("Start");

setTimeout(() => print("3s is passed"), 3000);
setTimeout(() => print("0s is passed"), 0);

for (let i=0; i<10000000; i++);

print("End")