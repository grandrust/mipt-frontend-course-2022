const { print } = require("../utils");
const fs = require("fs");

function read(fileName) {
    fs.readFile(fileName, (err, data) => {
        if (err) 
        {
            print(err);
            return;
        }

        print(data.toString());
    })
}

const fileName = process.argv[2];

// TODO: 
// if (validate(fileName))

print(`Content of ${fileName}:`);
read(fileName);