const { print } = require("./utils");


const todo = async () => {
    const value = await Promise.resolve(123);

    print(value);
}

todo();

// const resolvedPromise = Promise.resolve(123);
// print(resolvedPromise);


function http(data) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({data});
        }, 1000);
    })
}

function http1(data) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject("Error is happened");
        }, 1000);
    })
}

async function todo2() {
    const message = await http("Hello");
    print(message.data);
}

async function todo3() {
    // http1("Hello")
    //         .catch(er => todo(er))
    //         .then(result => tooodoo());

    try {
        const message = await http1("Hello");
        print(message.data);
    }
    catch(e) {
        print(e);
    }
}

todo2();
todo3();


// Example
// resolvedPromise
//     .then((val) => todo(val))
//     .then(resultTodo => todo2(resultTodo))
//     .catch(err => handleError(err));