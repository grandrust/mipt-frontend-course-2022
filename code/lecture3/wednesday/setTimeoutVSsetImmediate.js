const print = (message) => console.log(message);

// timeout_vs_immediate.js
setTimeout(() => {
    print('timeout');
  }, 0);
  
  setImmediate(() => {
    print('immediate');
  });