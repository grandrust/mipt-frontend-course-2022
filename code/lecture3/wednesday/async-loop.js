const print = (message) => console.log(message);

print("Start execution");

function sum(a, b) {
    return a + b;
}
print("Set timeout");
setTimeout(()=> print("Timeout Callback"), 0);

for (let i=0; i<10000000000; ++i)
{
    // 
}

print("Sum(2,6)");
print(sum(2,6));

// "Timeout Callback"