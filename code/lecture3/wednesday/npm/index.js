const images = require("images");

const compressImage = (imagePath, outputPath) => {
    images(imagePath)
        .size(400) 
        .save(outputPath, {              
            quality : 50   
        });
}
try {
    const inputImg = process.argv[2];
    const outImg = process.argv[3];
    compressImage(inputImg, outImg);
}
catch(ex)
{
    console.log(ex.message);
}