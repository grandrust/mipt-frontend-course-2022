const Koa = require('koa');
const app = new Koa();

app.use(async (ctx, next) => {
    console.log(`Got ${ctx.req.method} request`);

    await next();
});

app.use(async (ctx, next) => {

    if (ctx.query.apiKey !== "secret")
    {
        ctx.res.statusCode = 401;
        return;
    }

    await next();
});

app.use(async (ctx, next) => {
    console.log(`Got ${ctx.req.method} request`);
    
    await next();
});

app.use(async ctx => {
  ctx.body = 'Hello World';
});

app.listen(3000);