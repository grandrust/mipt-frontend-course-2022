const Calculator = require("./Calculator");

describe("Calculator", () => {
    test("sum 2 and 5 should be 7", () => {
        const calc = new Calculator();
        const result = calc.sum(2, 5);

        expect(result).toBe(7);
    });
});