import Student from "./Student";

const names: string[] = ["Ivan", "Sergey", "Petr"];
const students = names.map(name => new Student(name));

const root: HTMLElement = document.getElementById("root")!;
root.innerText = students.reduce((allText, student) => allText+= student.greating(), "");

const asyncJob = async () => {
    const response = await fetch("/api/posts");
    root.innerText += JSON.stringify(await response.json());
}

asyncJob();