var message = "Hello!!";
function greating(message) {
    console.log(message);
}
greating(message);
var Student = /** @class */ (function () {
    function Student(name, age) {
        var _this = this;
        this.name = name;
        this.age = age;
        // private name: string;
        // constructor(name)
        // {
        //     this.name = name;
        // }
        this.greating = function () {
            console.log("Hello, I'm ".concat(_this.name, "!"));
        };
    }
    return Student;
}());
//# sourceMappingURL=index.js.map