const message: string = "Hello!!";

function greating(message: string): void {
    console.log(message);
}

greating(message);

interface IPerson {
    greating: () => void
}

class Student implements IPerson {
    constructor(private name, private age)
    {}

    // private name: string;
    // constructor(name)
    // {
    //     this.name = name;
    // }

    greating = () => {
        console.log(`Hello, I'm ${this.name}!`);
    }

}

// new Student().greating();

// let s: string = "123";
// s = 123;