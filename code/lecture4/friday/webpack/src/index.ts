import { Student } from "./Student";
import "./styles.css";

const students: IPerson[] = [new Student<string>("Sergey", 25, "sdfds"), new Student<number>("Ivan", 25, 123), new Student<number>("Vasya", 25, 123)];
const root: HTMLElement = document.getElementById("root");

root.innerText = students.reduce((allGreetings, person) => allGreetings += person.greeting(), "");


async function getData() {
    const response = await fetch("/api/posts");
    const data = await response.json();

    root.innerText += "-----------"
    root.innerText += JSON.stringify(data);
}

getData();