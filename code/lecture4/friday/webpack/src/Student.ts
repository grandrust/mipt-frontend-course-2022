export class Person {
    constructor(protected name: string, protected age: number){
    }
    //...
}

export class Student<T> extends Person implements IPerson  {
    private key: T;

    constructor(name: string, age: number, key: T = null){
        super(name, age);
        this.key = key;
    }

    greeting(): string {
        return `Hello! I'm ${this.name}`;
    }   
}