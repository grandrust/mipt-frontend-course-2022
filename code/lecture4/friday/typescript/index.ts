const studentName: string = "Ivan";

console.log(`Hello! I'm ${studentName}`);

function log(message: string): void {
    console.log(message);
}

function trace() {
    return (...args) => {
        console.log("Method is called");
    }
}

interface IPerson {
    greeting: () => void
}

class Person {
    constructor(protected name: string, protected age: number){
    }
    //...
}

class Student<T> extends Person implements IPerson  {
    // private name: string;
    // private age: number;

    // constructor(name: string, age: number) {
    //     this.name = name;
    //     this.age = age;
    // }

    private key: T;
    constructor(name: string, age: number, key: T = null){
        super(name, age);
        this.key = key;
    }

    @trace()
    greeting(): void {
        debugger;
        console.log(`Hello! I'm ${this.name}`);
    }   
}

const persons: IPerson[] = [new Student<string>("Sergey", 25, "sdfds"), new Student<number>("Ivan", 25, 123)];

persons.forEach((p) => p.greeting());