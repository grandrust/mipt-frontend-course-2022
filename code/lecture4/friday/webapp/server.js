const Koa = require('koa');
const app = new Koa();

app.use(async (ctx, next) => {
    console.log(`Got ${ctx.request.method} request`);

    await next();
});

app.use(async (ctx, next) => {
    const startRequestTime = new Date();

    await next();

    console.log(`Time request: ${new Date() - startRequestTime}`);
});

app.use(async (ctx, next) => {

    if (ctx.query.apiCode !== "secret")
    {
        ctx.response.status = 401;
        return;
    }

    await next();

});

app.use(async ctx => {
  ctx.body = 'Hello World';
});

app.listen(3000);