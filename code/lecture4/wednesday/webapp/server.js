const Koa = require('koa');
const app = new Koa();

app.use(async (ctx, next) => {
    if (ctx.request.originalUrl == "/favicon.ico")
    {
        ctx.body = "iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACJSURBVEhL7ZC9CYBADEbP2iEEp7JxAPdwBZex02EcQ/0iBCzC/ZlTkDx4RYrLC+cMwzB+QQ1bRRtYwSAbPJQdoRe6bIbS41x32MEg9NULlJakStEBRqMRT44yFF+htDRkdpTJiT+OMilxtSgTE1ePMr54sSgjxYtHmXv8tShD8Qn212QYxnc4dwKskJKEHrOFUQAAAABJRU5ErkJggg==";
        return;
    }
    await next();
});

app.use(async (ctx, next) => {
    console.log("We got a request!");
    let startExecution = new Date();
    
    await next();

    let stopExecution = new Date() - startExecution;
    console.log(`It took: ${stopExecution}ms`);
});

app.use(async (ctx, next) => {
    if (ctx.query.apiCode == "12332")
    {
        await next();
    }
    else 
    {
        console.log("THe user is unauthorised");
        ctx.response.status = 401;
    }
});

app.use(async ctx => {
  ctx.body = 'Hello World';
});

app.listen(3000);