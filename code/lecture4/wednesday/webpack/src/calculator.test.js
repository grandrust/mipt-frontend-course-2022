const sum = require("./calculator");

describe("Calculator", ()=>{
    test("sum 2+5 is correct", ()=>{
        expect(sum(2,5)).toBe(7);
    })

    test("sum 2+5 shouldn't be 8", ()=>{
        expect(sum(2,5)).not.toBe(8);
    })
});