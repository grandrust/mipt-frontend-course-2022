import Student from "./Student";

const names: string[] = ["Ivan", "Petr", "Sergey"];
const students = names.map((name) => new Student(name));

for (let s of students)
{
    s.greating();
}

const root: HTMLElement = document.getElementById("root");
root.innerText = names.join("---^---");
