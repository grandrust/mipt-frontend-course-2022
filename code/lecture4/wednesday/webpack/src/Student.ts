export default class Student implements IPerson {
    constructor(private name: string)
    {}

    greating = () => {
        console.log(`Hello, I'm ${this.name} ...`);
    }
}