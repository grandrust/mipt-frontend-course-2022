const message: string = "Hello world!";

interface IPerson {
    greating: () => void
}

class Student implements IPerson {
    constructor(private name: string)
    {}

    greating = () => {
        console.log("sdf");
    }
}

function Todo(person: IPerson)
{
    person.greating();
}

function Greating(message: string): void {
    console.log(message);
}

Greating(message);

Todo(new Student("Ivan"))