# Pet Project

1. Refactor [task 4](../lecture5/pet-project.html) using reduxjs.

    1. Reduxjs has been added to the project.
    2. Interaction with the store is carried out using reduxjs.


2. [_optional_] Create real-time web application "laser pointer".

See more details [here](https://gitlab.com/grandrust/mipt-frontend-course-2022/-/blob/main/code/lecture6/README.md)