# Pet Project

Refactor [task 3](../lecture4/pet-project.html) using react.

1. Adjust typescript and webpack configurations to use react.
2. Rewrite coffee shop with react


See more details [here](https://gitlab.com/grandrust/mipt-frontend-course-2022/-/blob/main/code/lecture5/README.md)